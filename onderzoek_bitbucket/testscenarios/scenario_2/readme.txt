

acties:
=======
- basis = 1 in Centrale Repository
- Henk en Nico gaan lokaal met zelfde file aan de slag
- Nico merged eerst zijn wijzigingen naar CP
- Daarna Henk

Verwachting:
============
- BitBucket blokkeert de merge en tikt Henk op de vingers
  geeft minimaal de volgende info: 
     - waar zitten de verschillen
     - door wie, wanneer zijn deze gemaakt 


Verschil met scenario 1:
========================
Nico en Henk doen aanpassingen in verschillende objecten binnen de package
Nico in de functie (a geeft b en andersom)
Henk in de procedure die afhankelijk is van de functie
