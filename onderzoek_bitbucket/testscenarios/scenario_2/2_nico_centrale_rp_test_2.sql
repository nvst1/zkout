create or replace package test_2
as

  -- History-----
  --v01  created


function get_a_b (p_a_b in varchar2)  
  return varchar2;

procedure do_a_b (p_a_b in varchar2) ; 

end test_2;
/



create or replace package body test_2
as

  -- History-----
  --v01  created

function get_a_b (p_a_b in varchar2)  
  return varchar2
is
begin

  if p_a_b = 'A'
  then
    return 'B'; 
  elsif p_a_b = 'B'
  then
    return 'A'; 
  end if;    

  return null;

end;  

procedure do_a_b (p_a_b in varchar2)  
is
begin

  if get_a_b (p_a_b) = 'A'
  then
    dbms_output.put_line('doAstuff'); 
  elsif get_a_b (p_a_b) = 'B'
  then
    dbms_output.put_line('doBstuff');  
  else
    dbms_output.put_line('error');
  end if;

end;

end test_2;
/

