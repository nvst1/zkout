DECLARE
-- test procedure voor GIT
--    procedure kan runnen op alle ZKOUT instances en geeft als resultaat "dossid: 758046305"


-- test   N   v_02 l_nr toevoegen met inhoud relaid_debiteur
--        H   v_02 l_nr toevoegen met inhoud relaid_eiser
--        daarna
--        N   comitten en pushen
--        daarna H comitten en pushen
-- wat gebeurd er?? 
--  gewenst   melding conflict   met Nico als laatste comitter

-- History-----
--v01  created
--v02  toevoegen l_nr inhoud relaid_eiser


   l_dossid NUMBER;

   l_nr  number;

  CURSOR test1 (p_dosint IN VARCHAR2) IS
    select dossid,
           RELAID_EISER           
      from dossiers
      where dossiernummer_int = p_dosint;
      
BEGIN
    OPEN  test1('X34100654');
    FETCH test1 INTO l_dossid, l_nr;
    CLOSE test1;

dbms_output.put_line('dossid: '||l_dossid);
dbms_output.put_line('l_nr: '|| to_char(l_nr)); 

END;      