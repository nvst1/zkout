DECLARE
-- test procedure voor GIT
--    procedure kan runnen op alle ZKOUT instances en geeft als resultaat "dossid: 758046305"


-- test   p1   aan cursor toevoegen andere kolom uit dossid en toevoegen aan output
--        p2   hernoemen van de cursor
--        daarna de code laten mergen

-- History-----
--v01  created
--v02  toevoegen l_nr


   l_dossid NUMBER;

   l_nr  number;

  CURSOR test1 (p_dosint IN VARCHAR2) IS
    select dossid,
           RELAID_DEBITEUR           
      from dossiers
      where dossiernummer_int = p_dosint;
      
BEGIN
    OPEN  test1('X34100654');
    FETCH test1 INTO l_dossid, l_nr;
    CLOSE test1;

dbms_output.put_line('dossid: '||l_dossid);
dbms_output.put_line('l_nr: '|| to_char(l_nr)); 

END;      